
#include "CollisionManager.h"


void CollisionManager::AddCollisionType(const CollisionType type1, const CollisionType type2, OnCollision callback)
{
	Collision c;
	//If type1 collision gives less damage than type2 collision, type1 will be damaged.
	c.Type1 = (type1 < type2) ? type1 : type2;
	//If type1 collision gives more damage than type2 collision, type2 will be damaged
	c.Type2 = (type1 > type2) ? type1 : type2;
	//When a collision occurs, the custom callback is used to respond
	c.Callback = callback;

	//Adds Collision to vector
	m_collisions.push_back(c);
}

void CollisionManager::CheckCollision(GameObject *pGameObject1, GameObject *pGameObject2)
{
	CollisionType t1 = pGameObject1->GetCollisionType();
	CollisionType t2 = pGameObject2->GetCollisionType();

	//Check to see if there is a Collision
	//If t1 and t2 are the same collisiontype or t1 or t2's collisiontype is none, return nothing
	if (t1 == t2 || t1 == CollisionType::NONE || t2 == CollisionType::NONE) return;

	bool swapped = false; //Set bool swapped to false
	if (t1 > t2)
	{
		std::swap(t1, t2); //swap t1 and t2
		swapped = true; //bool swapped changed to true
	}

	m_nonCollisionIt = m_nonCollisions.begin();
	for (; m_nonCollisionIt != m_nonCollisions.end(); m_nonCollisionIt++) //Loop through nonCollision vector until end
	{
		NonCollision nc = *m_nonCollisionIt; //Derefences NonCollision Pointer
		if ((nc.Type1 == t1 && nc.Type2 == t2)) return; //If both t1 and t2 are NonCollision, return nothing;
	}

	m_collisionIt = m_collisions.begin();
	for (; m_collisionIt != m_collisions.end(); m_collisionIt++) //Loop through Collision vector until end
	{
		Collision c = *m_collisionIt;
		if ((c.Type1 == t1 && c.Type2 == t2))
		{
			Vector2 difference = pGameObject1->GetPosition() - pGameObject2->GetPosition(); //Gets the difference in position between to two objects in the collision

			float radiiSum = pGameObject1->GetCollisionRadius() + pGameObject2->GetCollisionRadius(); //Gets radiiSum by adding the collision radius of the objects in the collision
			float radiiSumSquared = radiiSum * radiiSum; //Gets radiiSumSquared by multiplying radiiSum by radiiSum

			//If the length squared of the vector of the difference in the position of the obejcts is less than or equal to the radiiSumSquared
			if (difference.LengthSquared() <= radiiSumSquared) 
			{
				if (!swapped) c.Callback(pGameObject1, pGameObject2); //If the objects weren't swapped, return this callback
				else c.Callback(pGameObject2, pGameObject1); //If the objects were swapped, return this callback
			}
			return;
		}
	}
	
	//If none of these are true, create a AddNonCollisitionType; 
	AddNonCollisionType(t1, t2);
}

void CollisionManager::AddNonCollisionType(const CollisionType type1, const CollisionType type2)
{
	NonCollision nc;
	//If type1 collision has less collision damage than type2 collision, type1 won't be damaged.
	nc.Type1 = (type1 < type2) ? type1 : type2;
	//If type1 collision has more collision damage than type2 collision, type2 won't be damaged.
	nc.Type2 = (type1 > type2) ? type1 : type2;
	//Adds nonCollision to vector
	m_nonCollisions.push_back(nc);
}